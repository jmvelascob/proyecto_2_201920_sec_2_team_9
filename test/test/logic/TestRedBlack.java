package test.logic;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import model.data_structures.RedBlackLiteBST;
import model.logic.TravelTime;


public class TestRedBlack {

	private RedBlackLiteBST<Integer, String> rb;
	
	@Before
	public void setUp2()
	{
		rb=new RedBlackLiteBST<Integer,String>();
		
	}
	
	@Before
	public void setUp1()
	{
		rb=new RedBlackLiteBST<Integer, String> ();
		rb.put(1, "1");
		rb.put(2, "2");
		rb.put(3, "3");
		rb.put(4, "4");
		rb.put(6, "6");
		rb.put(10, "10");
	}
	@Before
	public void setUp5()
	{
		rb=new RedBlackLiteBST<Integer, String> ();
		rb.put(1, "1");

	}
	
	@Test
	public void tabla()
	{
		setUp1();
		int x = rb.min();
		int y = rb.max();

		assertEquals(1, x);
		assertEquals(10, y );
		assertEquals(true, rb.check());
		assertEquals(6, rb.size());
		assertEquals(false, rb.isEmpty());
		assertEquals("6", rb.get(6));
		assertEquals(false, rb.contains(11));
		assertEquals(true, rb.contains(10));
		assertEquals(2, rb.height());
		int s= rb.keysInRange(1, 8).next();
		assertEquals(1,s);
		String z= rb.valuesInRange(1, 8).next();
		assertEquals("1", z);
		
	}
	
	@Test
	public void vacio()
	{
		setUp2();
		assertEquals(true, rb.isEmpty());
		assertEquals(null, rb.min());
		assertEquals(null, rb.max());
	}
	@Test
	public void unelemnto()
	{
		setUp5();
		assertEquals(0, rb.height());
	}
	
	
	
}

