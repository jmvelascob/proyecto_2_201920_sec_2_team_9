package test.logic;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.MaxColaCP;
import model.logic.TravelTime;

public class TestMaxColaCP {

	private MaxColaCP<TravelTime> cola;
	
	@Before
	public void setUp()
	{
		cola= new MaxColaCP<TravelTime>();
		TravelTime uno= new TravelTime(0,1, 2, 2, 3.5, 4.5, 3.2, 1.1 );
		TravelTime dos= new TravelTime(0,28, 2, 2, 1, 4.5, 3.2, 1.1 );
		TravelTime tres= new TravelTime(0,36, 2, 2, 8, 4.5, 3.2, 1.1 );
		TravelTime cuatro= new TravelTime(0,101, 2, 2, 20, 4.5, 3.2, 1.1 );

		
		cola.agregar(uno);

		cola.agregar(dos);

		cola.agregar(tres);

		cola.agregar(cuatro);


	}
	
	@Before
	public void setUp2()
	{
		cola= new MaxColaCP<TravelTime>();
	}

	@Test
	//CREA UNA COLA (COMIENZA VAC�A) Y LUEGO LA VAC�A
	public void agregarYVaciar()
	{
		setUp();
		assertEquals(4, cola.darNumElementos());
		cola.sacarMax();
		assertEquals(3, cola.darNumElementos());

	}
	
	//aGREGA A UNA COLA VAC�A
	@Test
	public void vac�o()
	{
		setUp2();
		assertEquals(true, cola.esVacia());
		assertEquals(0, cola.darNumElementos());
		TravelTime x= new TravelTime(0,4, 2, 2, 28, 4.5, 3.2, 1.1 );
		cola.agregar(x);
		assertEquals(false, cola.esVacia());
		assertEquals(1, cola.darNumElementos());
	}
	
	//SACA EL M�XIMO DE UNA COLA

	@Test
	public void sacarMaximo()
	{
		setUp();
		assertEquals(1, cola.sacarMax().darSourceid(), 0);
		assertEquals(3, cola.darNumElementos());
	}
	

	
	
}
