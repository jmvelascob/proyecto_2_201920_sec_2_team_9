package controller;

import java.util.ArrayList;

import model.data_structures.ArregloDinamico;
import model.data_structures.MaxColaCP;

import java.util.Iterator;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import model.data_structures.RedBlackLiteBST;
import model.logic.MVCModelo;
import model.logic.NodoCoordenadas;
import model.logic.NodoVial;
import model.logic.TravelTime;
import model.logic.Zona;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	private final static int N=20;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.cargarJSON();
				modelo.cargarTXT();
				modelo.cargarCSV();
				System.out.println("N�mero de zonas le�das en el JSON: "+ modelo.darnumjson());
				break;

			case 2:
				System.out.println("Ingresar n�mero de letras: ");
				String num= lector.next();
				MaxColaCP colaaa=modelo.letrasFrecuentes(Integer.parseInt(num));
				int contip=0;
				int elmini=colaaa.darNumElementos();
				while(contip<Integer.parseInt(num))
				{
					ArregloDinamico arr= (ArregloDinamico) colaaa.darMax();
					System.out.println("LETRA: "+((Zona) arr.darElemento(0)).darSCANombre().charAt(0));
					for(int i=0; i<arr.darTamano();i++)
					{
						Zona zon=(Zona) arr.darElemento(i);
						System.out.println(zon.darSCANombre());
					}
					contip++;
					colaaa.sacarMax();
					
				}
				
				break;
			case 3:
				System.out.println("Introduzca la longitud");
				Double lon = Double.parseDouble(lector.next());
				System.out.println("Introduzca la latitud");
				Double lat = Double.parseDouble(lector.next());
				Iterator iter = modelo.buscarNodos(lon, lat);
				int cont = 0;
				while(iter!= null && iter.hasNext())
				{
					NodoCoordenadas nodo = (NodoCoordenadas)iter.next();
					System.out.println("Longitud: " + nodo.darLongitud() + " Latitud: " + nodo.darLatitud() + " Nombre Zona: " + nodo.darZona());
					cont++;
				}
				System.out.println("El numero de nodos fue de " + cont);
				break;
			
			case 4:
				System.out.println("Ingresar limite bajo: ");
				String dat= lector.next();
				int limitebajo=Integer.parseInt(dat);
				System.out.println("Ingresar limite alto: ");
				String d= lector.next();
				int limitealto= Integer.parseInt(d);
				RedBlackLiteBST<Double, TravelTime> arbol=modelo.tiemposPromedioRango(limitebajo, limitealto);
				int conte=0;
				Iterator<TravelTime> itere=arbol.valuesInRange(arbol.min(), arbol.max());
				MaxColaCP<TravelTime> cola= new MaxColaCP<TravelTime>();
				while(itere.hasNext())
				{
					cola.agregar(itere.next());
				}
				while(cola.darMax()!=null&&conte<20)
				{
					TravelTime viaje=cola.darMax();
					System.out.println(("Viaje: Zona de origen: "+viaje.darSourceid()+ ", Zona de destino: "+viaje.darDtid()+", Mes: "+ viaje.darDow()+ ", Tiempo promedio: "+viaje.darMean_travel_time()));
					viaje=cola.darMax();
					cola.sacarMax();
					conte++;
				}
				break;
				
			case 5:
				System.out.println("Ingresar n�mero de letras: ");
				String numm= lector.next();
				MaxColaCP co= modelo.zonasAlNorte(Integer.parseInt(numm));
				int conteo=0;
				while(conteo<Integer.parseInt(numm))
				{
					MaxColaCP p=(MaxColaCP) co.sacarMax();
					NodoCoordenadas z=(NodoCoordenadas) p.darMax();
					System.out.println("Nombre: "+z.darZona()+", Longitud: "+ z.darLongitud()+ ", Latitud: "+z.darLatitud());
					co.sacarMax();
					conteo++;
				}
				break;
				
			case 6:
				System.out.println("Introduzca la longitud");
				Double lonnj = Double.parseDouble(lector.next());
				System.out.println("Introduzca la latitud");
				Double lattj = Double.parseDouble(lector.next());
				Iterator iterrpe = modelo.nodosLocalizacion(lonnj, lattj);
				int conttp = 0;
				while(iterrpe!=null && iterrpe.hasNext())
				{
					NodoVial nodo = (NodoVial)iterrpe.next();
					System.out.println("Id Nodo: " + nodo.darid() + " Longitud: " + nodo.darlongitud() + " Latitud: " + nodo.darlatitud());
					conttp++;
				}
				System.out.println("El numero de nodos fue de " + conttp);
				break;
				
			case 7:
				System.out.println("Ingresar limite bajo: ");
				String dat1= lector.next();
				int limitebajo1=Integer.parseInt(dat1);
				System.out.println("Ingresar limite alto: ");
				String d1= lector.next();
				int limitealto1= Integer.parseInt(d1);
				RedBlackLiteBST<Double, TravelTime> arbol1=modelo.buscarTiemposEspera(limitebajo1, limitealto1);
				int cont1=0;
				Iterator<TravelTime> iterre=arbol1.valuesInRange(arbol1.min(), arbol1.max());
				MaxColaCP<TravelTime> colaa= new MaxColaCP<TravelTime>();
				while(iterre.hasNext())
				{
					colaa.agregar(iterre.next());
				}
				while(colaa.darMax()!=null&&cont1<20)
				{
					TravelTime viaje=colaa.darMax();
					System.out.println(("Viaje: Zona de origen: "+viaje.darSourceid()+ ", Zona de destino: "+viaje.darDtid()+", Mes: "+ viaje.darDow()+ ", Desviaci�n: "+viaje.darStandard_deviation_travel_time()));
					viaje=colaa.darMax();
					colaa.sacarMax();
					cont1++;
				}
				break;
			case 8:
				System.out.println("Introduzca el source del viaje");
				int source = Integer.parseInt(lector.next());
				System.out.println("Introduzca la hora");
				int hora = Integer.parseInt(lector.next());
				Iterator it = modelo.tiemposPromedioZonaYHora(source, hora);
				if(it==null)
				{
					System.out.println("No hay viajes que salgan desde ese source a esa hora");
				}
				while(it!=null && it.hasNext())
				{
					TravelTime viaje = (TravelTime)it.next();
					System.out.println("Zona Origen: " + viaje.darSourceid() + " Zona Destino: " + viaje.darDtid() + " Hora: " + viaje.darDow() + " Tiempo Promedio: " + viaje.darMean_travel_time());
				}
				break;	
			case 9:
				System.out.println("Ingresar zona: ");
				String g=lector.next();
				int zonap= Integer.parseInt(g);
				System.out.println("Ingresar hora inicial: ");
				String gg=lector.next();
				int horainiciaal=Integer.parseInt(gg);
				System.out.println("Ingresar hora final: ");
				String ggg=lector.next();
				int horafinaal=Integer.parseInt(ggg);

				RedBlackLiteBST l=modelo.tiemposPromedioZonaYRango(zonap, horainiciaal, horafinaal);
				Iterator ita=l.valuesInRange(l.min(), l.max());
				while(ita.hasNext())
				{
					TravelTime t=(TravelTime) ita.next();
					System.out.println("Zona de origen: "+t.darSourceid()+ ", Zona de destino: "+t.darDtid()+", Hora: "+t.darDow()+" Tiempo promedio: "+t.darMean_travel_time() );
				}
				break;
			case 10:
				System.out.println("Ingrese el numero de zonas que desea");
				int N = Integer.parseInt(lector.next());
				ArregloDinamico ar = modelo.zonasMasNodosFrontera(N);
				for(int i=0;i<N;i++)
				{
					Zona z = (Zona)ar.darElemento(i);
					System.out.println("Nombre zona: " + z.darSCANombre() + " Numero de nodos: " + z.darNumeroNodos());
				}
				break;
			case 11:
				modelo.graficaASCII();
				break;
			default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
