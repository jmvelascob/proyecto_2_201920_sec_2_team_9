package model.logic;
import com.google.gson.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.Cola;
import model.data_structures.HashLinearProbing;
import model.data_structures.MaxColaCP;
import model.data_structures.RedBlackLiteBST;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {
	
	private Zona zona;
	private int contadorZonas;
	private String instruccion;
	private ArregloDinamico<Zona> arr1;
	private ArregloDinamico<TravelTime>viajeshoras;
	private ArregloDinamico<TravelTime> viajesmeses;
	private ArregloDinamico<TravelTime> viajesdias;
	private ArregloDinamico<TravelTime> viajeshoras2;
	private ArregloDinamico<TravelTime> viajesmeses2;
	private ArregloDinamico<TravelTime> viajesdias2;
	private ArregloDinamico<NodoVial> txt;
	private String path;
	int mayorSource;
	
	public MVCModelo()
	{
		txt=new ArregloDinamico<NodoVial>(10000);
		
		viajesmeses=new ArregloDinamico<TravelTime>(10000);
		viajesdias=new ArregloDinamico<TravelTime>(10000);
		viajeshoras=new ArregloDinamico<TravelTime>(10000);
		viajesmeses2=new ArregloDinamico<TravelTime>(10000);
		viajesdias2=new ArregloDinamico<TravelTime>(10000);
		viajeshoras2=new ArregloDinamico<TravelTime>(10000);
		arr1= new  ArregloDinamico<Zona>(10000);

	}

	public void cargarTXT()
	{
		int numt=0;
		CSVReader reader = null;
		try
		{
			reader = new CSVReader(new FileReader("./data/Nodes_of_red_vial-wgs84_shp.txt"));
			try
			{
				String[] x =reader.readNext();
			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}
			for(String[] nextLine : reader) 
			{
				NodoVial dato = new NodoVial(Integer.parseInt(nextLine[0]), Double.parseDouble(nextLine[1]), Double.parseDouble(nextLine[2]));
				txt.agregar(dato);
				numt++;
			}
			System.out.println("N�mero de nodos TXT: "+numt);
		}
		
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null  ) 
			{
				try 
				{
					reader.close();


				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public void cargarCSV()
	{
		CSVReader reader = null;
		CSVReader reader2=null;
		CSVReader reader3=null;
		CSVReader reader4 = null;
		CSVReader reader5=null;
		CSVReader reader6=null;
		try {
			
		
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			reader2 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv"));
			reader3 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv"));
			reader4 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv"));
			reader5 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-WeeklyAggregate.csv"));
			reader6 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv"));

			try{
				String[] x =reader.readNext();
				String[] y =reader2.readNext();
				String[] z =reader3.readNext();
				String[] p =reader4.readNext();
				String[] t =reader5.readNext();
				String[] s =reader6.readNext();

			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}
			
			for(String[] nextLine : reader) 
			{
				TravelTime dato = new TravelTime( 1 ,Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				viajeshoras.agregar(dato);

				if(dato.darSourceid()>mayorSource)
				{
					mayorSource = dato.darSourceid();
				}
			}
			System.out.println("N�mero de viajes archivo horas trimestre 1: "+viajeshoras.darTamano());

			
			for(String[] nextLine : reader2) 
			{
				TravelTime dato = new TravelTime(1, Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				viajesdias.agregar(dato);
				
			}
			System.out.println("N�mero de viajes archivo d�as trimestre 1: "+viajesdias.darTamano());

			for(String[] nextLine : reader3) 
			{
				TravelTime dato = new TravelTime( 1, Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				viajesmeses.agregar(dato);
				
			}
			System.out.println("N�mero de viajes archivo meses trimestre 1: "+viajesmeses.darTamano());
		}
		
//			for(String[] nextLine : reader4) 
//			{
//				TravelTime dato = new TravelTime( 2 ,Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
//				viajeshoras2.agregar(dato);
//			}
//			
//			System.out.println("N�mero de viajes archivo horas trimestre 2: "+viajeshoras2.darTamano());
//			
//			for(String[] nextLine : reader5) 
//			{
//				TravelTime dato = new TravelTime(2, Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
//				viajesdias2.agregar(dato);
//				
//			}
//			System.out.println("N�mero de viajes archivo d�as trimestre 2: "+viajesdias2.darTamano());
//
//			for(String[] nextLine : reader6) 
//			{
//				TravelTime dato = new TravelTime( 2, Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
//				viajesmeses2.agregar(dato);
//			}
//			System.out.println("N�mero de viajes archivo meses trimestre 2: "+viajesmeses2.darTamano());
//		}
		
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null && reader2!=null && reader3!=null && reader4 != null && reader5!=null && reader6!=null ) 
			{
				try 
				{
					reader.close();
					reader2.close();
					reader3.close();	
					reader4.close();
					reader5.close();
					reader6.close();

				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public ArregloDinamico<TravelTime> darlinelmes()
	{
		return viajesmeses;
	}

	public ArregloDinamico<TravelTime> darlinelhoras()
	{
		return viajeshoras;
	}
	public ArregloDinamico<TravelTime> darlineldias()
	{
		return viajesdias;
	}
	public ArregloDinamico<NodoVial>  darheap()
	{
		return txt;
	}
	

	public void cargarJSON() 
	{
		try
		{
			JsonParser parser = new JsonParser();
			String path = "./data/bogota_cadastral.json";
	        FileReader fr = new FileReader(path);
	        JsonElement datos = parser.parse(fr);
	        json(datos);
		}
		catch(Exception e)
		{
			System.out.println("No se pudo leer el json");
		}
	}public void json(JsonElement elemento) {
	    if (elemento.isJsonObject()) {
	       // System.out.println("Es objeto");
	        JsonObject obj = elemento.getAsJsonObject();
	        java.util.Set<java.util.Map.Entry<String,JsonElement>> entradas = obj.entrySet();
	        java.util.Iterator<java.util.Map.Entry<String,JsonElement>> iter = entradas.iterator();
	        while (iter.hasNext()) 
	        {
	            java.util.Map.Entry<String,JsonElement> entrada = iter.next();
	           // System.out.println("Clave: " + entrada.getKey());
	            if(entrada.getKey().equals("coordinates"))
	            {
	            	instruccion = "coordinates";
	            	zona = new Zona();
	            	contadorZonas++;
	            }
	            else if(entrada.getKey().equals("type"))
	            {
	            	instruccion = "type";
	            }
	            else if(entrada.getKey().equals("geometry"))
	            {
	            	instruccion = "geometry";
	            }
	            else if(entrada.getKey().equals("cartodb_id"))
	            {
	            	instruccion = "cartodb_id";
	            }
	            else if(entrada.getKey().equals("scacodigo"))
	            {
	            	instruccion = "scacodigo";
	            }
	            else if(entrada.getKey().equals("scatipo"))
	            {
	            	instruccion = "scatipo";
	            }
	            else if(entrada.getKey().equals("scanombre"))
	            {
	            	instruccion = "scanombre";
	            }
	            else if(entrada.getKey().equals("shape_leng"))
	            {
	            	instruccion = "shape_leng";
	            }
	            else if(entrada.getKey().equals("shape_area"))
	            {
	            	instruccion = "shape_area";
	            }
	            else if(entrada.getKey().equals("MOVEMENT_ID"))
	            {
	            	instruccion = "MOVEMENT_ID";
	            }
	            else if(entrada.getKey().equals("DISPLAY_NAME"))
	            {
	            	instruccion = "DISPLAY_NAME";
	            }
	            else if(entrada.getKey().equals("properties"))
	            {
	            	instruccion="properties";
	            }
	            //System.out.println("Valor:");
	            json(entrada.getValue());
	        }
	 
	    } else if (elemento.isJsonArray()) {
	        JsonArray array = elemento.getAsJsonArray();
	        //System.out.println("Es array. Numero de elementos: " + array.size());
	        java.util.Iterator<JsonElement> iter = array.iterator();
	        while (iter.hasNext()) {
	            JsonElement entrada = iter.next();
	            json(entrada);
	        }
	    } else if (elemento.isJsonPrimitive()) {
	       // System.out.println("Es primitiva");
	        JsonPrimitive valor = elemento.getAsJsonPrimitive();
	        if (valor.isBoolean())
	        {
	           // System.out.println("Es booleano: " + valor.getAsBoolean());
	        } 
	        else if (valor.isNumber()) 
	        {	
	        	if(instruccion.equals("coordinates"))
	        	{
	        		agregarCoordenadas(valor.getAsDouble());
	        	}
	        	else if(instruccion.equals("MOVEMENT_ID"))
	        	{
	        		agregarMovementID(valor.getAsInt());
	        	}
	        	else if(instruccion.equals("shape_leng"))
	            {
	            	agregarShapeLen(valor.getAsDouble());
	            }
	            else if(instruccion.equals("shape_area"))
	            {
	            	agregarShapeArea(valor.getAsDouble());
	            }
	            //System.out.println("Es numero: " + valor.getAsNumber());
	            
	        } else if (valor.isString()) 
	        {
	        	if(instruccion.equals("scanombre"))
	        	{
	        		agregarSCANombre(valor.getAsString());
	        	}
	        	else if (instruccion.equals("MOVEMENT_ID"))
	        	{
	        		agregarMovementID(valor.getAsInt());
	        	}
	            //System.out.println("Es texto: " + valor.getAsString());
	        }
	    } else if (elemento.isJsonNull()) {
	        System.out.println("Es NULL");
	    } else {
	        System.out.println("Es otra cosa");
	    }
	}
	
	public void agregarCoordenadas(double valor)
	{
		NodoCoordenadas primer = zona.darPrimerasCoordenadas();
		while(primer.darSiguiente()!=null)
		{
			primer = primer.darSiguiente();
		}
		if(primer.darLongitud()==0)
		{
			primer.cambiarLongitud(valor);
		}
		else if(primer.darLatitud()==0)
		{
			primer.cambiarLatitud(valor);
			NodoCoordenadas nuevo = new NodoCoordenadas();
			primer.cambiarSiguiente(nuevo);
		}
	}
	private int numjson=0;
	public void agregarMovementID(int valor)
	{
		zona.cambiarMovementID(valor);
		arr1.agregar(zona);
		zona.ponerleNombreNodos();

		numjson++;
	}
	
	public void agregarSCANombre(String valor)
	{
		zona.cambiarSCANombre(valor);
	}
	
	public void agregarShapeLen(double valor)
	{
		zona.cambiarShapeLen(valor);
	}
	
	public void agregarShapeArea(double valor)
	{
		zona.cambiarShape_Area(valor);
	}

	public int darnumjson()
	{
		return numjson;
	}

	
	//req2
	public  MaxColaCP letrasFrecuentes(int n)
	{
		ArregloDinamico<Zona>[] arreglo;
		arreglo=new ArregloDinamico[26];
		ArregloDinamico<Zona> todas=arr1;
		int cont=0;
		
		for (int u=0; u<26;u++)
		{
			arreglo[u]= new ArregloDinamico<Zona>(100);
		}
		while(cont<arr1.darTamano())
		{
			Zona zona=todas.darElemento(cont);
			
			for(int i=0; i<26; i++)
			{
				if(Character.getNumericValue(zona.darSCANombre().toUpperCase().charAt(0))==i+10)
				{
					arreglo[i].agregar(zona);
				}
			}
			cont++;
		}
		
		MaxColaCP cola= new MaxColaCP();
		for(int j=0; j<arreglo.length;j++)
		{
			cola.agregar(arreglo[j]);
		}
	


		return cola;
	}
	
	//req3
	public Iterator buscarNodos(double lon, double lat)
	{
		HashLinearProbing hash = new HashLinearProbing();
		String lo = cortarDeci1(lon,3);
		String la = cortarDeci1(lat,3);
		String x = lo + "," + la;
		int cont = 0;
		while(cont<arr1.darTamano())
		{
			Zona zona = (Zona)arr1.darElemento(cont);
			NodoCoordenadas actual = zona.darPrimerasCoordenadas();
			while(actual.darSiguiente()!=null)
			{
				String long1 = cortarDeci1(actual.darLongitud(),3);
				String lat1 = cortarDeci1(actual.darLatitud(),3);
				String fina = long1 + "," + lat1;
				hash.putInSet(fina, actual);
				actual = actual.darSiguiente();
			}
			cont++;
		}
		Iterator iter = hash.getSet(x);
		return iter;
	}
	
	
	//req4
	public RedBlackLiteBST<Double, TravelTime> tiemposPromedioRango(int limitebajo, int limitealto)
	{
		
		ArregloDinamico<TravelTime> arr= viajesmeses;
		RedBlackLiteBST<Double, TravelTime> arbolredb= new RedBlackLiteBST<Double, TravelTime>();
		
		int cont=1;
		while(cont<arr.darTamano())
		{
			TravelTime viaje=arr.darElemento(cont);
			Double llave= viaje.darMean_travel_time();
			if(viaje.darMean_travel_time()>limitebajo && viaje.darMean_travel_time()<limitealto)
			{
				arbolredb.put(llave, viaje);
			}
			cont++;
		}
		return arbolredb;
	}
	
	//req5
	public MaxColaCP<Zona> zonasAlNorte(int n)
	{
		MaxColaCP[] arreglo;
		arreglo=new MaxColaCP[arr1.darTamano()];
		
		for (int p=0; p<arr1.darTamano();p++)
		{
			arreglo[p]= new MaxColaCP();
		}
		
		int cont=0;
		while(cont<arr1.darTamano())
		{
			NodoCoordenadas nod=arr1.darElemento(cont).darPrimerasCoordenadas();
			while(nod!=null)
			{
				arreglo[cont].agregar(nod);
				nod=nod.darSiguiente();
			}
			cont++;
		}
		int conta=0;
		MaxColaCP devolver=new MaxColaCP();
		while(conta<arreglo.length)
		{
			devolver.agregar(arreglo[conta]);
			conta++;
		}
		return devolver;
	}
	
	//req6
	public String cortarDeci1(double x, int lar)
	{
		String num = Double.toString(x);
		String concat = "";
		int cont = 0;
		int c = 0;
		boolean encontro = false;
		boolean termino = false;
		while(cont < lar && termino == false)
		{
			if(c+1==num.length())
			{
				termino = true;
			}
			char m = num.charAt(c);
			String k = Character.toString(m);
			c++;
			if(k.equals("."))
			{
				encontro = true;
			}
			if(encontro == true)
			{
				cont++;
			}
			concat = concat + k;
		}
		return concat;
	}
	public Iterator nodosLocalizacion (double longitud, double latitud)
	{
		HashLinearProbing hash = new HashLinearProbing();
		String lo = cortarDeci1(longitud,4);
		String la = cortarDeci1(latitud,4);
		String x = lo + "," + la;
		int cont = 0;
		while(cont<txt.darTamano())
		{
			
			NodoVial nodo = (NodoVial)txt.darElemento(cont);
			String long1 = cortarDeci1(nodo.darlongitud(),4);
			String lat1 = cortarDeci1(nodo.darlatitud(),4);
			String fina = long1 + "," + lat1;
			hash.putInSet(fina, nodo);
			cont++;
		}
		Iterator iter = hash.getSet(x);
		return iter;
	}

	//req7
	public RedBlackLiteBST<Double, TravelTime> buscarTiemposEspera(int limbajo, int limalto)
	{
		
		ArregloDinamico<TravelTime> arr= viajesmeses;
		RedBlackLiteBST<Double, TravelTime> arbol= new RedBlackLiteBST<Double, TravelTime>();
		
		int cont=1;
		while(cont<arr.darTamano())
		{
			TravelTime viaje=arr.darElemento(cont);
			Double llave= viaje.darStandard_deviation_travel_time();
			if(viaje.darStandard_deviation_travel_time()>limbajo && viaje.darStandard_deviation_travel_time()<limalto)
			{
				arbol.put(llave, viaje);
			}
			cont++;
		}
		return arbol;	
		}
	
	//req 8
	public Iterator tiemposPromedioZonaYHora(int id, int hora)
	{
		HashLinearProbing<String, TravelTime> hash = new HashLinearProbing<String, TravelTime>();
		for(int i=0;i<viajeshoras.darTamano();i++)
		{
			TravelTime viajeeeee = (TravelTime)viajeshoras.darElemento(i);
			String key = viajeeeee.darSourceid() + "," + viajeeeee.darDow();
			hash.putInSet(key, viajeeeee);
		}
		for(int i=0;i<viajeshoras2.darTamano();i++)
		{
			TravelTime viajee = (TravelTime)viajeshoras2.darElemento(i);
			String keyy = viajee.darSourceid() + "," + viajee.darDow();
			hash.putInSet(keyy, viajee);
		}
		String keyyy = id + "," + hora;
		Iterator iter = hash.getSet(keyyy);
		return iter;
	}
	
	//req9
	public RedBlackLiteBST<Double, TravelTime> tiemposPromedioZonaYRango(int zona, int horainicial, int horafinal)
	{
		ArregloDinamico<TravelTime> uno=viajeshoras;
		ArregloDinamico<TravelTime> dos=viajeshoras;
		RedBlackLiteBST<Double, TravelTime> arbol= new RedBlackLiteBST<Double, TravelTime>();
		int a=0;
		int b=0;
		while(a<uno.darTamano())
		{
			if(uno.darElemento(a).darDtid()==zona && uno.darElemento(a).darDow()>=horainicial && uno.darElemento(a).darDow()<=horafinal)
			{
			double llave=uno.darElemento(a).darMean_travel_time();
			arbol.put(llave, uno.darElemento(a));
			}
			a++;
		}
		while(b<dos.darTamano())
		{
			if(dos.darElemento(b).darDtid()==zona && dos.darElemento(b).darDow()>=horainicial && dos.darElemento(b).darDow()<=horafinal)
			{
			double llave=dos.darElemento(b).darMean_travel_time();
			arbol.put(llave, dos.darElemento(b));
			}
			b++;
		}
		return arbol;
	}
	
	//req10
	public ArregloDinamico zonasMasNodosFrontera(int n)
	{
		MaxColaCP cola = new MaxColaCP();
		int cont=0;
		while(cont<arr1.darTamano())
		{
			Zona zona = (Zona)arr1.darElemento(cont);
			zona.actualizarNumeroNodos();
			int numeroNodos = zona.darNumeroNodos();
			cola.agregar(zona);
			cont++;
		}
		ArregloDinamico arreglo = new ArregloDinamico(10);
		for(int i=0; i<n;i++)
		{
			Zona zon = (Zona)cola.sacarMax();
			arreglo.agregar(zon);
		}
		return arreglo;
	}
	
	//req11
	public void graficaASCII()
	{
		int total1 = 0;
		int real1 = 0;
		
		ArregloDinamico arreglo = viajeshoras;
		for(int f=0; f<viajeshoras2.darTamano();f++)
		{
			arreglo.agregar(viajeshoras2.darElemento(f));
		}
		
		System.out.println("Porcentaje de datos faltantes por zona");
		HashLinearProbing hash = new HashLinearProbing();
		for(int j=0;j<arreglo.darTamano();j++)
		{
			TravelTime viaje = (TravelTime)arreglo.darElemento(j);
			String llave = viaje.darSourceid() + "," + viaje.darDtid();
			hash.putInSet(llave, viaje);
		}
		for(int m=1; m<=20;m++)
		{
			total1 = 0;
			real1 = 0;
			for(int f=1;f<=20;f++)
			{
				String key = m + "," + f;
				Iterator iter = hash.getSet(key);
				while(iter!= null && iter.hasNext())
				{
					real1++;
					iter.next();
				}
				if(m!=f)
				{
					total1 = total1 + 24;
				}
			}
			double porc1 = 0;
			if(total1 == 0)
			{
				porc1 = 100;
			}
			else
			{
				porc1 = 100 - (real1 * 100 / total1);
			}
			int aster1 = (int)porc1/2;
			String concat1 = "";
			for(int t=0;t<aster1;t++)
			{
				concat1 = concat1 + "*";
			}
			System.out.println(m + "| " + concat1);
		}
	}
}
	
	
	

