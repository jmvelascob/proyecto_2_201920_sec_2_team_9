package model.logic;

import java.util.Iterator;

public class TravelTime implements  Comparable<TravelTime>{
	
	private int sourceid;
	
	private int dow;

	private int dtid;
	
	private double mean_travel_time;
	
	private double standard_deviation_travel_time;
	
	private double geometric_mean_travel_time;
	
	private double geometric_standard_deviation_travel_time;
	
	private int trimestre;
	
	public TravelTime(int trim, int Psourceid, int Pdtid,int hodd, double Pmean_travel_time, double Pstandard_deviation_travel_time, double Pgeometric_mean_travel_time, double Pgeometric_standard_deviation_travel_time)
	{
		trimestre=trim;
		dow= hodd;
		sourceid = Psourceid;
		dtid = Pdtid;
		mean_travel_time = Pmean_travel_time;
		standard_deviation_travel_time = Pstandard_deviation_travel_time;
		geometric_mean_travel_time = Pgeometric_mean_travel_time;
		geometric_standard_deviation_travel_time = Pgeometric_standard_deviation_travel_time;
	}
	
	public int darTrim()
	{
		return trimestre;
	}
	
	public int darSourceid()
	{
		return sourceid;
	}
	
	public int darDtid()
	{
		return dtid;
	}
	

	public double darMean_travel_time()
	{
		return mean_travel_time;
	}
	
	public double darStandard_deviation_travel_time()
	{
		return standard_deviation_travel_time;
	}
	
	public double darGeometric_mean_travel_time()
	{
		return geometric_mean_travel_time;
	}
	
	public double darGeometric_standard_deviation_travel_time()
	{
		return geometric_standard_deviation_travel_time;
	}
	public int darDow()
	{
		return dow;
	}


	@Override
	public int compareTo(TravelTime o) {
		// TODO Auto-generated method stub
		if(sourceid==o.darSourceid())
		{
		if(dtid==o.darDtid())
			return 0;
		else if(dtid - o.darDtid()>0)
			return -1;
		else 
			return 1;
		}
		if(sourceid-o.darSourceid()>0)
			return -1;
		else
			return 1;
		}
	

	}



