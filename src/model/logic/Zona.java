package model.logic;

public class Zona implements Comparable<Zona>{

	private int movementID;
	private String scaNombre;
	private double shape_Len;
	private double shape_Area;
	private int numeroNodos;
	private NodoCoordenadas primeraCoordenada;
	
	public Zona()
	{
		primeraCoordenada = new NodoCoordenadas();
	}
	
	public int darMovementID()
	{
		return movementID;
	}
	
	public String darSCANombre()
	{
		return scaNombre;
	}
	
	public void cambiarMovementID(int pm)
	{
		movementID = pm;
	}
	
	public void cambiarSCANombre(String nombre)
	{
		scaNombre = nombre;
	}
	
	public void cambiarShapeLen(double len)
	{
		shape_Len = len;
	}
	
	public void cambiarShape_Area(double len)
	{
		shape_Area = len;
	}
	
	public NodoCoordenadas darPrimerasCoordenadas()
	{
		return primeraCoordenada;
	}
	
	public void actualizarNumeroNodos()
	{
		if(primeraCoordenada!=null)
		{
			NodoCoordenadas actual = primeraCoordenada;
			while(actual!=null)
			{
				numeroNodos++;
				actual = actual.darSiguiente();
			}
		}
	}
	
	public int darNumeroNodos()
	{
		return numeroNodos;
	}
	
	public void ponerleNombreNodos()
	{
		NodoCoordenadas actual = primeraCoordenada;
		while(actual!=null)
		{
			actual.cambiarZona(scaNombre);
			actual = actual.darSiguiente();
		}
	}

	@Override
	public int compareTo(Zona arg0) 
	{
		if(arg0.darNumeroNodos()==numeroNodos)
		{
			return 0;
		}
		else if(arg0.darNumeroNodos()<numeroNodos)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	

}
