package model.data_structures;

import java.awt.List;
import java.util.ArrayList;
import java.util.Iterator;

import model.logic.TravelTime;

public class HashLinearProbing <K extends Comparable<K> ,V extends Comparable<V>> implements ITablaHash<K, V>  {
	
	public final static int INIT_CAPACITY=4;
	private int capacidad;
	private int duplas;
	private int datos;
	private K[] llaves;
	private Cola<V>[] objetos;
	private int rehashes;

    /**
     * Initializes an empty symbol table.
     */
    public HashLinearProbing() {
        this(INIT_CAPACITY);
    }

	public HashLinearProbing(int m)
	{
		capacidad=m;
		datos=0;
		llaves= (K[]) new Comparable[m];
		objetos= new Cola[m];
	}
	
	public int darDuplas()
	{
		return duplas;
	}
	public int darDatos()
	{
		return datos;
	}

	public boolean esVacio()
	{
		return darDatos()==0;
	}
	
	private int hash(K lla)
	{
		return (lla.hashCode() & 0x7fffffff) % capacidad ;
	}
	
	private void resize(int capac)
	{
		HashLinearProbing<K, V> temp= new HashLinearProbing<K, V>(capac);
		for(int i=0;i<capacidad;i++)
		{
			if(llaves[i]!=null)
			{
				Node<V>temp1= objetos[i].darPrimer();
				while(temp1!=null)
				{
				temp.putInSet(llaves[i], temp1.darData());
				temp1=temp1.darSiguiente();
				}
			}
		}
		llaves=temp.llaves;
		objetos=temp.objetos;
		capacidad=temp.capacidad;
		duplas=temp.duplas;
		datos=temp.datos;
		
	}
	
	
	@Override
	public void putInSet(K key, V value) {
		// TODO Auto-generated method stub
		if(key==null)
		{
			System.out.println("primer argumento es nulo");
		}


		double d=duplas;
		double c=capacidad*0.75;
		int captemp=capacidad*2;
		if(d>=c)
		{

				resize(captemp);
				rehashes++;

		}
		
		int temp=hash(key);
		int i=temp;
		do
		{
			if(llaves[i]==null)
			{
			objetos[i]=new Cola<V>();
			llaves[i]=key;
			objetos[i].enqueue(value);
			datos++;
			duplas++;
			return;
			}
			if(llaves[i].equals(key))
			{
				objetos[i].enqueue(value);
				datos++;
				return;
			}
			i = (i + 1) % capacidad;
		}
		while(i!=temp);
	}

	@Override
	public Iterator<V> getSet(K key) {
		// TODO Auto-generated method stub
		if(key==null)
		{
			System.out.println("La llave es nula");
		}
		for (int i=hash(key); llaves[i]!=null; i=(i+1)%capacidad)
		{
			if(llaves[i].equals(key))
			{
				return objetos[i].iterator();
			}
		}
			return null;
		}
	
	public boolean contains(K key)
	{
		if (key==null)
		System.out.println("Argumento es nulo");
		return getSet(key)!=null;
	}
	
	public int darcapacidad()
	{
		return capacidad;
	}

	@Override
	public Iterator<V> deleteSet(K key) {
		// TODO Auto-generated method stub
		
		if(key==null)
			System.out.println("Argumento es nulo");
		if(!contains(key))
		{
			System.out.println("Llave vac�a");
		}
		
		int i=hash(key);
		Cola<V> temppp=objetos[i];

		while (!key.equals(llaves[i]))
		{
			i=(i+1)%capacidad;
		}
		
		int tempi=objetos[i].darTama�o();
		llaves[i]=null;
		objetos[i]=null;
		
		i=(i+1)%capacidad;
		while(llaves[i]!=null)
		{
			K llavearehash=llaves[i];
			Cola<V> valoresRehash= objetos[i];
			int ll=objetos[i].darTama�o();
			llaves[i]=null;
			objetos[i]=null;
			datos-=ll;
			duplas--;
			Node<V>temp1= valoresRehash.darPrimer();
			while(temp1.darSiguiente()!=null)
			{
			putInSet(llavearehash, temp1.darData());
			temp1.darSiguiente();
			}
			i=(i+1)%capacidad;
		}
		duplas --;
		datos-=tempi;
		if (duplas > 0 && duplas <= capacidad/8) resize(capacidad/2);
		return temppp.iterator();
	}
	
	public int darRehashes()
	{
		return rehashes;
	}

	public double darFactorCarga()
	{
		return (double)duplas/capacidad;
	}
	@Override
	public Iterator<K> keys() {
		// TODO Auto-generated method stub
		Cola<K> cola=new Cola<K>();
		for(int i=0; i<capacidad;i++)
		{
			if(llaves[i]!=null)
			{
				cola.enqueue(llaves[i]);
			}
		}
		return cola.iterator();
	}

	
}
