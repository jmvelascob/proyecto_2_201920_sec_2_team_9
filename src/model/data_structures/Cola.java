package model.data_structures;

import java.util.Iterator;


public class Cola <T extends Comparable<T>> 
	{

		
		private Node<T> primer;
		private Node<T> ultimo;
		
		//Constructor de la lista
		public Cola()
		{
			primer = null;
			ultimo = null;
		}
		
		public boolean estaVacia()
		{
			if(primer==null)
			{
				return true;
			}
			return false;
		}
		
		public Node<T> darPrimer()
		{
			return primer;
		}
		
		public Node<T> darUltimo()
		{
			return ultimo;
		}
		
		public int darTama�o()
		{
			if(estaVacia())
			{
				return 0;
			}
			else
			{
				Node<T> actual = primer;
				int cont = 0;
				while(actual != null)
				{
					cont++;
					actual = actual.darSiguiente();
				}
				return cont;
			}
		}

		public void enqueue(T dato) 
		{
			if(primer == null)
			{
				Node<T> pr = new Node<T>(dato);
				primer = pr;
				ultimo = pr;
			}
			else
			{
				Node<T> ult = ultimo;
				Node<T> nuevo = new Node<T>(dato);
				ult.cambiarSig(nuevo);
				ultimo = nuevo;
			}	
		}


		public Node<T> dequeue() {
			// TODO Auto-generated method stub
			if(primer!=null)
			{
				Node<T> elimin = primer;
				primer = primer.darSiguiente();
				return elimin;
			}
			return null;		}


		public Iterator<T> iterator()
		{
			return new ListIterator();
		}
		
		private class ListIterator implements Iterator<T>
		{
			private Node<T> actual=primer;
			
			public boolean hasNext()
			{
				return actual!=null;
			}
			public T next()
			{
				T dat = actual.darData();
				actual=actual.darSiguiente();
				return dat;
			}
			public void remove(){}
			
		}
		

	}


