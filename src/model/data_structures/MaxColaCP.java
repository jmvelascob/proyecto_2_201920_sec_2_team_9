package model.data_structures;

public class MaxColaCP <T extends Comparable<T>> implements Comparable {

	int numerodeda;
	Node<T> primero;
	Node<T>ultimo;

	
	public MaxColaCP()
	{
		numerodeda=0;
		primero=null;
		ultimo=null;
	}
	
	public int darNumElementos()
	{
		return numerodeda;
	}
	
	public Node<T> darPrimero()
	{
		return primero;
	}
	
	public boolean esVacia()
	{
		if(numerodeda==0) return true;
		else return false;
	}
	
	public T darMax()
	{
		return primero.darData();
	}
	
	public T sacarMax()
	{
		if(esVacia())
		{
			return null;
		}
		else if(darNumElementos()>2){
			Node<T> elimin = primero;
			Node<T> p=primero.darSiguiente();
			primero = null;
			primero=p;
			numerodeda--;
			return elimin.data;
		}
		else if(darNumElementos()==2)
		{
			primero.cambiarSig(ultimo);
			Node<T> elimin = primero;
			primero=primero.darSiguiente();
			numerodeda--;
			return elimin.data;
		}
		else
		{
			T da=primero.data;
			primero=null;
			ultimo=null;
			numerodeda--;
			return da;
		}
		}
	
	public void agregar(T elemento)
	{

		if(esVacia()) 
		{
			primero= new Node<T>(elemento);
			ultimo=new Node<T>(elemento);
			numerodeda++;
		}
		else
		{
			if(ultimo.darData().compareTo(elemento)>0)
			{
				Node<T> ult = ultimo;
				Node<T> nuevo = new Node<T>(elemento);
				ult.cambiarSig(nuevo);
				ultimo = nuevo;
				numerodeda++;
			}
			else if(primero.darData().compareTo(elemento)==0 )
			{
				Node<T> nuevo = new Node<T>(elemento);
				Node<T> prim= primero;
				nuevo.cambiarSig(prim);
				primero=nuevo;
				numerodeda++;
			}
			else if( primero.darData().compareTo(elemento)<0)
			{
				Node<T> nuevo = new Node<T>(elemento);
				Node<T> prim= primero;
				nuevo.cambiarSig(prim);
				primero=nuevo;
				numerodeda++;
			}
			else 
			{
				Node<T> nuevo = new Node<T>(elemento);
				Node<T> nodoo=primero;
				while(nodoo.darSiguiente()!=null)
				{
					if(nodoo.darData().compareTo(nuevo.data)>0 && nodoo.darSiguiente().darData().compareTo(elemento)<0)
					{
						nuevo.cambiarSig(nodoo.darSiguiente());
						nodoo.cambiarSig(nuevo);
						numerodeda++;
					}
					nodoo=nodoo.darSiguiente();
				}
			}
		}
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		T dataa= (T)((MaxColaCP) o).darMax();
		return primero.data.compareTo(dataa);
	}

	
	}

